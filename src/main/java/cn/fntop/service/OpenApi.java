package cn.fntop.service;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.http.*;


/**
 * @author fn
 * @description
 * @date 2023/7/31 22:06
 */
public interface OpenApi {

    @HTTP(method = "GET")
    Single<Object> get(@Url String suffix);

    @HTTP(method = "GET")
    Single<Object> get(@Url String suffix, @Path(value = "name") String name);

    @HTTP(method = "GET")
    Single<Object> get(@Url String suffix, @Query(value = "query") String query, boolean isQuery);

    @HTTP(method = "POST", hasBody = true)
    Single<Object> post(@Url String suffix, @Body Object object);

    @Multipart
    @HTTP(method = "POST", hasBody = true)
    Single<Object> post(@Url String suffix,  @Query(value = "folder") String folder, @Part MultipartBody.Part... file);

}
