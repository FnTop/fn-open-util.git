package cn.fntop.service;
import okhttp3.MultipartBody;

public class OpenApiService extends OpenApiServiceAbstract {
    public Object get(String suffix) {
        return execute(api.get(suffix));
    }

    public Object get(String suffix, String path) {
        return execute(api.get(suffix, path));
    }

    public Object getOnQuery(String suffix, String query) {
        return execute(api.get(suffix, query, true));
    }

    public Object post(String suffix, Object obj) {
        return execute(api.post(suffix, obj));
    }

    public Object post(String suffix, Object obj, boolean isFile, String folder, MultipartBody.Part... file) {
        if (isFile) {
            return execute(api.post(suffix, folder, file));
        }
        return execute(api.post(suffix, obj));
    }

}
